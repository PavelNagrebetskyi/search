import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class MainPage {
        private WebDriver driver;
    public MainPage(WebDriver driver) {
        this.driver = driver;
    }

    private By grvSubWidget = By.className("grv-sub-widget middle-x top not-branded");
    private By subdialog = By.className("sub-dialog-btn block_btn");
    private By search = By.xpath("/html/body/header/div[4]/section/form");
    private By searchFrom1 = By.xpath("/html/body/header/div[4]/section/form/div[1]/div/div[1]");
    private By searchFrom2 = By.xpath("/html/body/header/div[4]/section/form/div[1]/div/div[2]");
    private By searchFromInput1 = By.xpath("//*[@id=\"md-input-7l2dy3no9\"]");
    private By searchFromInput2 = By.xpath("//*[@id=\"md-input-muwnddloz\"]");
}
