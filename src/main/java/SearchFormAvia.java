import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.openqa.selenium.support.ui.ExpectedConditions;

public class SearchFormAvia {
    private WebDriver driver;
    private WebDriverWait waitUntilElementIsReadyTimeLimit;
    private static String city1;
    public SearchFormAvia(WebDriver driver)
                    {
                        this.driver = driver;
                    }

    public By getSearchFrom() {
        return searchFrom;
    }
    //body > header > div.container.mb-2 > section > form
    private By searchFrom = By.xpath("/html/body/header/div[4]/section/form");
    private By searchFormFieldDirection = By.xpath("/html/body/header/div[4]/section/form/div[1]/div/div[1]");
    private By searcherAutocompleteActive = By.xpath("/html/body/header/div[4]/section/form/div[1]/div/div[1]/div/section");
    //class="autocomplete active";
    private By searchFormInput = By.xpath("/html/body/header/div[4]/section/form/div[1]/div/div[1]/div/section/div/div/input");
    private By inputOneIsNotEmpty = By.xpath("/html/body/header/div[4]/section/form/div[1]/div/div[1]/div");
    private By inputOneAutocomplecontainer = By.xpath("body > header > div.container.mb-2 > section > form > div.main-form__pane > div > div:nth-child(1) > div > section > div");
    private By getInputOneAutocompleteList = By.xpath("/html/body/header/div[4]/section/form/div[1]/div/div[1]/div/section/div/ul");
    private By getInputOneAutocompleteListItem1 = By.xpath("/html/body/header/div[4]/section/form/div[1]/div/div[1]/div/section/div/ul/li[1]/div");

    private By SubscribeMainPageBTN= By.className("sub-dialog-btn block_btn");
    private By SubscribeMainPage = By.className("grv-sub-widget middle-x top not-branded");


    public void Subscribe(){
        waitUntilElementIsReadyTimeLimit = (new WebDriverWait(driver, 15));
        waitUntilElementIsReady(SubscribeMainPage,"class", "grv-sub-widget middle-x top not-branded");
        waitUntilElementIsReady(SubscribeMainPage,"style", "--main-color:#0d4d96; --text-color:#545458");
        waitUntilElementIsReady(SubscribeMainPage,"class", "sub-dialog-btn block_btn");


        waitUntilElementIsReadyTimeLimit.until(ExpectedConditions.visibilityOfElementLocated(SubscribeMainPage));
        waitUntilElementIsReadyTimeLimit.until(ExpectedConditions.visibilityOfElementLocated(SubscribeMainPage));
        waitUntilElementIsReadyTimeLimit.until(ExpectedConditions.elementToBeClickable(SubscribeMainPageBTN));
        driver.findElement(SubscribeMainPageBTN).click();
        waitUntilElementIsReadyTimeLimit.until(ExpectedConditions.invisibilityOfElementLocated(SubscribeMainPageBTN));
        waitUntilElementIsReadyTimeLimit.until(ExpectedConditions.invisibilityOfElementLocated(SubscribeMainPage));
    }
    public void enterDeparturePoint(String city1){
        waitUntilElementIsReadyTimeLimit.until(ExpectedConditions.elementToBeClickable(searchFormFieldDirection));
        driver.findElement(searchFormFieldDirection).click();
        waitUntilElementIsReady(searcherAutocompleteActive,"class", "\"autocomplete active\"");
        System.out.println(getAttributeValueByAttributeName(searcherAutocompleteActive,"class"));
        driver.findElement(searchFormInput).sendKeys(city1);
        waitUntilElementIsReady(inputOneIsNotEmpty,"class", "md-field md-theme-default md-focused md-has-value");
        waitUntilElementIsReadyTimeLimit.until(ExpectedConditions.visibilityOfElementLocated(inputOneAutocomplecontainer));
        waitUntilElementIsReadyTimeLimit.until(ExpectedConditions.visibilityOfElementLocated(getInputOneAutocompleteList)) ;
        driver.findElement(getInputOneAutocompleteListItem1).click();
    }

    public String getAttributeValueByAttributeName(By attributeValue, String attributeName) {
        return driver.findElement(attributeValue).getAttribute(attributeName);
    }
    public void waitUntilElementIsReady(By checkElementBy, String attributeName, String value ){
        waitUntilElementIsReadyTimeLimit = (new WebDriverWait(driver, 15));
        waitUntilElementIsReadyTimeLimit.until(ExpectedConditions.attributeContains( checkElementBy, attributeName, value));
        System.out.println(value); }


}
