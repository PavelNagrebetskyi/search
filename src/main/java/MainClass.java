import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import java.util.concurrent.TimeUnit;

public class MainClass {
    static WebDriver driver;

    public MainClass() {

    }

    public static void main(String[] args) {
        System.setProperty("webdriver.chrome.driver", "D:\\chromedriver\\chromedriver.exe");
        driver = new ChromeDriver();
        //driver.manage().timeouts().implicitlyWait(50, TimeUnit.SECONDS);
        driver.manage().window().maximize();
        driver.get("https://tickets.ru/");
        SearchFormAvia searchFormAvia = new SearchFormAvia(driver);
        searchFormAvia.Subscribe();
        By form = searchFormAvia.getSearchFrom();
        String sea = searchFormAvia.getAttributeValueByAttributeName(form,"action");

        searchFormAvia.waitUntilElementIsReady(searchFormAvia.getSearchFrom(), "action", "https://avia.tickets.ru/m/preloader/0/1%7C0%7C0/A");
        System.out.println(sea);


        driver.quit();

//        MainPage mainPage = new MainPage(driver);
//        mainPage.register("testusername", "qweqew@cxcv.com", "qweqwe2SDD33");
//        mainPage.register("testusername", "qweqew@cxcv.com", "qweqwe2SDD33");
    }
}
